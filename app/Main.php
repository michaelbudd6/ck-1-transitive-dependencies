<?php
declare(strict_types=1);

namespace App;

use Illuminate\Support\Collection;

final class Main
{
	private CONST DEPENDENCIES = [
		'A' => ['B','C'],
		'B' => ['C','E'],
		'C' => ['G'],
		'D' => ['A','F'],
		'E' => ['F'],
		'F' => ['H']
	];

	public function __construct()
	{
		$this->workOutAndPrintDependencies(self::DEPENDENCIES);
	}

	/**
	 * @param array $dependencyGroups
	 *
	 * @return void
	 */
	private function workOutAndPrintDependencies(array $dependencyGroupsMaster): void
	{
		$masterDependencies = [];

		foreach ($dependencyGroupsMaster as $dependencyParent => $dependencies) {
			foreach ($dependencies as $dependency) {
				$masterDependencies[$dependencyParent][] = $dependency;
			}
		}

		$newMasterDependencies = [];

		foreach ($masterDependencies as $dependency => $dependencies) {
			$transitiveDependencies = $this->recursivelyAppendDependencies($masterDependencies, $dependency);
			$transitiveDependencies = array_unique($transitiveDependencies);
			sort($transitiveDependencies);
			$newMasterDependencies[$dependency] = $transitiveDependencies;
		}

		var_dump($newMasterDependencies);		
	}

	/**
	 * @param array  $masterDependencies
	 * @param string $parentDependency
	 *
	 * @return array
	 */
	private function recursivelyAppendDependencies(
		array $masterDependencies, 
		string $parentDependency): array
	{
		$dependencies = [];

		if (isset($masterDependencies[$parentDependency])) {
			$dependencies = $masterDependencies[$parentDependency];
			foreach ($dependencies as $dependency) {
				if (isset($masterDependencies[$dependency])) {
					$dependencies = array_merge($dependencies, $this->recursivelyAppendDependencies($masterDependencies, $dependency));
				}
			}
		}

		return $dependencies;
	}
}