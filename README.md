# Transitive Dependencies  

## House Rules:  

- If you'd like to share your solution, that could be really helpful for others, but you don't have to! If you do, please create a branch and push when ready.
- Feel free to use another language, but preferrably something that is fairly expressive. Assembly, for example, might not be particularly helpful.
- If you're commenting on someone elses solution, please be respectful, constructive criticism only.
- No touching of the hair and face    

Please follow instructions here:

http://codekata.com/kata/kata18-transitive-dependencies/

